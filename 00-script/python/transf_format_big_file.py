import argparse
import sys
import os
import zipfile
import pandas as pd
import anndata as ad

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description="Find the count table in the h5ad file")

# Add arguments for the CSV file name
parser.add_argument('file_name', metavar='file_name', type=str,
                    help='name of the h5ad file to analyse')
parser.add_argument('--output', metavar='output', type=str,
                    help='name of the output directory', default='output_folder')
parser.add_argument('--output-name', metavar='output_name', type=str,
                    help='output file name', default='counts.csv')
args =  parser.parse_args()

adata = None
print("Starting now")
# Load file .h5ad
if zipfile.is_zipfile(args.file_name):
    with zipfile.ZipFile(args.file_name, 'r') as zip_ref:
        # Get list of filenames in zip file
        file_list = zip_ref.namelist()
        # Find the file name that ends with ".h5ad"
        h5ad_file_name = next((file_name for file_name in file_list if file_name.endswith('.h5ad')), None)
        if h5ad_file_name is None:
        # If no .h5ad file was found, display an error message and exit
            print("No file .h5ad has been find.")
            sys.exit()
        # Extract the .h5ad file
        file_h5ad_to_read = zip_ref.extract(h5ad_file_name)
        adata = ad.read_h5ad(file_h5ad_to_read)
print("Work in progres...")
# adata = ad.read_h5ad(args.file_name)
# Extract the gene count matrix
counts = pd.DataFrame(adata.X.toarray(), columns=adata.var_names, index=adata.obs_names)
print("count extracted")
#reverse the table
counts_transpose = counts.T

# Create the output directory if it does not exist
if not os.path.exists(args.output):
    os.makedirs(args.output)
print("count ready to be writen")
print("number of column: ", counts_transpose.shape[1])


# Cut the table in three parts
third1 = int(counts_transpose.shape[1] / 3)
third2 = 2 * third1

counts_part1 = counts_transpose.iloc[:, :third1]
counts_part2 = counts_transpose.iloc[:, third1:third2]
counts_part3 = counts_transpose.iloc[:, third2:]

print("Counts_part1:\n", counts_part1,"\n")
print("Counts_part2:\n", counts_part2,"\n")
print("Counts_part3:\n", counts_part3,"\n")

# Export the gene count matrix
# counts_part1.to_csv(os.path.join(args.output, args.output_name), sep=",")
# counts_part2.to_csv(os.path.join(args.output, args.output_name), sep=",")
counts_part3.to_csv(os.path.join(args.output, args.output_name), sep=",")
print(f"Count table from {args.file_name} extracted.")

# # cut the table in half to write it in two different files
# half_col = int(counts_transpose.shape[1]/2)
# counts_part1 = counts_transpose.iloc[:, :half_col]
# counts_part2 = counts_transpose.iloc[:, half_col:]
# print("Counts_part1:\n", counts_part1,"\n")
# print("Counts_part2:\n", counts_part2)
#
# # Export the gene count matrix
# #counts_part1.to_csv(os.path.join(args.output, args.output_name), sep=",")
# counts_part2.to_csv(os.path.join(args.output,args.output_name), sep=",")
# print(f"Count table from {args.file_name} extracted.")



# cut the table in third to write it in three different files
# third_col = int(counts_transpose.shape[1]/3)
# counts_part1 = counts_transpose.iloc[:, :third_col]
# counts_part2 = counts_transpose.iloc[:, third_col+1:third_col+third_col]
# counts_part3 = counts_transpose.iloc[third_col+third_col+1, :]
# print("Counts_part1:\n", counts_part1,"\n")
# print("Counts_part2:\n", counts_part2, "\n")
# print("Counts_part3:\n", counts_part3)

# # Export the gene count matrix
# counts_part1.to_csv(os.path.join(args.output, args.output_name), sep=",")
# counts_part2.to_csv(os.path.join("organ_part2/" ,args.output_name), sep=",")
# print(f"Count table from {args.file_name} extracted.")
