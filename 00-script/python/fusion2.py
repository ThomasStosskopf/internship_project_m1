import pandas as pd
import os
import argparse

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description="Merge all csv file from the selected directory")

# Add argument
parser.add_argument('-directory', metavar = 'directory', type = str,
                    help = 'Path of the directory where all the csv files are.')
parser.add_argument('-output_path', metavar = 'output_path', type= str,
                    help = 'name of the output directory', default = 'output_directory_merge')
parser.add_argument('-output_name', metavar = 'output_name', type = str,
                    help = 'name of the output file', default = 'Count_merge.csv')
args = parser.parse_args()


def remove_file_suffix(file_path):
    return file_path.rsplit('/', 1)[0] + '/'

# function to add '/' at the end of the directory path
def add_trailing_slash(string):
    if string.endswith('/'):
        return string
    else:
        return string + '/'

# Create the output directory if it does not exist
if not os.path.exists(args.output_path):
    os.makedirs(args.output_path)



folder_path = args.directory

print(folder_path)
folder_path = add_trailing_slash(folder_path)
#folder_path = remove_file_suffix(folder_path)
# Loop into the directory to get all the csv file
csv_files = [f for f in os.listdir(folder_path) if f.endswith('.csv')]
# Take the first csv file to get its first column
first_csv = pd.read_csv(folder_path+csv_files[0])
# Rename the first column "gene_id"
first_csv = first_csv.rename(columns={first_csv.columns[0]: 'gene_id'})
# Create the new DataFrame that will contain all the others
# if os.path.exists(os.path.join(args.output_path,args.output_name)):
#     df_fuse = pd.read_csv(args.output_path,args.output_name)
# else:
#     df_fuse = pd.DataFrame()
#     # Copys the first column "gene_id" into the new DataFrame
#     # So we can join all the other table based on this column
#     df_fuse['gene_id'] = first_csv['gene_id'].copy
# # Loop to read all csv file and merge it in the df_fuse DataFrame.
#
# df_fuse = df_fuse.merge(first_csv, on = "gene_id" , how = "outer")
# print(df_fuse)
df_fuse = pd.DataFrame()
# Copys the first column "gene_id" into the new DataFrame
    # So we can join all the other table based on this column
df_fuse['gene_id'] = first_csv['gene_id'].copy
for file in csv_files:
    print(file)
    df_temp = pd.read_csv(folder_path+file)
    df_temp = df_temp.rename(columns={df_temp.columns[0]: 'gene_id'})
    df_fuse = df_fuse.merge(df_temp, on = "gene_id" , how = "outer")
print("bouclefini")
df_fuse.set_index('gene_id', inplace=True)
# # Write df_fuse into a file
df_fuse.to_csv(os.path.join(args.output_path, args.output_name), sep = ",")
