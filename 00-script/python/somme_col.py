import pandas as pd


# Create an ArgumentParser object
parser = argparse.ArgumentParser(description="Find the count table in the h5ad file")

# Add arguments for the CSV file name
parser.add_argument('file_name', metavar='file_name', type=str,
                    help='name of the h5ad file to analyse')
parser.add_argument('--output', metavar='output', type=str,
                    help='name of the output directory', default='output_folder')
parser.add_argument('--output-name', metavar='output_name', type=str,
                    help='output file name', default='counts.csv')
args =  parser.parse_args()

df = pd.read_csv(args.file_name , index_col = 0)
# Renommer la dernière colonne en utilisant le nom de la première colonne
#df = df.rename(columns={df.columns[-1]: df.columns[0]})
print(df)

columns = df.columns

for col in columns:
    print(col)
    col_split = col.split(".")[0]
    print(col_split)
    df = df.rename(columns = {col : col_split})

df = df.groupby(df.columns, axis=1).sum()

print(df)
df.to_csv(os.path.join(args.output, args.output_name), sep=",")



# # Faire l'addition des deux colonnes
# df_sum = df["TSP14_TS_endothelial"] + df["TSP14_TS_endothelial.1"]
#
# df_new = pd.DataFrame()
#
# # Ajouter la colonne somme au dataframe
# df_new["TSP14_TS_endothelial"] = df_sum
#
# # Afficher le résultat
# print(df_new)
# df_new.to_csv("deux_colmerged.csv", sep=",")
