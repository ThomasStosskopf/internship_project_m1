# script map
# library
import pandas as pd
import numpy as np

## Créer le premier dataframe avec 100 gènes et 50 échantillons
genes = ['Gene_'+str(i+1) for i in range(100)]
samples = ['Sample_'+str(i+1) for i in range(50)]
df = pd.DataFrame(np.random.randint(0, 100, size=(100, 50)), columns=samples, index=genes)
print("First table: count table \n",df)
# Créer le deuxième dataframe avec les noms d'échantillons et les caractéristiques correspondantes
sample_names = ['Sample_'+str(i+1) for i in range(50)]
df_samples = pd.DataFrame(sample_names, columns=['Sample'])
sample_features = ['cell_epithelial', 'cell_B', 'cell_immu', 'cell_alpha', 'cell_beta']
df_samples['Feature'] = pd.Series(sample_features * 10)[:50]
print("Second table: metadata \n",df_samples)
# Remplacer les noms d'échantillons par leur caractéristique correspondante dans le premier dataframe
for i in range(len(sample_names)):
    df.rename(columns={sample_names[i]: df_samples.loc[i, 'Feature']}, inplace=True)

# Afficher le nouveau dataframe avec les caractéristiques d'échantillons comme noms de colonnes
print("Third table: count table transf \n",df)

# Somme des colonnes avec des noms identiques
df = df.groupby(df.columns, axis=1).sum()

# Afficher le nouveau dataframe sans les colonnes en double
print("Fourth table: summeryse \n",df)

#
# metadata = pd.read_csv("metadata02.csv")
#
# print(metadata.head())
# print(metadata.tail())


#print(count.head())
# count_3x3 = count.iloc[:3, :3]
# raw_nb = count_3x3.shape[0]
#
# print(count_3x3)
# print("raw nb is ", count_3x3.shape[0], "\n col nb is ", count_3x3.shape[1])
