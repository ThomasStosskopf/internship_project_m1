import pandas as pd
import numpy as np
import argparse
import os

# Créer un objet ArgumentParser
parser = argparse.ArgumentParser(description="Sum the columns for each row of a \
CSV file containing omics data.")

# Ajouter un argument pour le nom du fichier CSV
parser.add_argument('-file_name', metavar='file_name', type=str, help='name of the CSV file to analyse')
parser.add_argument('-file_donor', metavar='file_donor', type=str, help='name of the CSV file with donor tag')
parser.add_argument('--output_path', metavar='output_path',
                    type=str, default='output_path.csv',
                    help='path of the output file (default: output_path.csv)')
parser.add_argument('--output-name', metavar='output_name', type=str,
                    help='output file name', default='counts.csv')
# Définition du chunksize pour le fichier des comptages de gènes
parser.add_argument('--chunk_size', metavar='chunk_size', type=int, default=1000, help='The size of the chunks to use to read the CSV file (default: 1000)')

args =  parser.parse_args()
# Lecture du premier fichier CSV contenant les données de comptage par morceaux
#chunks = pd.read_csv('counttest2.csv', index_col=0, chunksize=chunksize)
# Create the output directory if it does not exist
if not os.path.exists(args.output_path):
    os.makedirs(args.output_path)
# Initialisation du DataFrame de résultats
df_counts = pd.DataFrame()

# Lire le fichier CSV en chunks et sommer les colonnes pour chaque ligne
for chunk in pd.read_csv(args.file_name, index_col=0, chunksize=args.chunk_size):
    # Lecture du deuxième fichier CSV contenant les informations de features des échantillons
    df_features = pd.read_csv(args.file_donor)

    # Remplacement des noms de colonnes du premier tableau par les features correspondantes
    chunk = chunk.rename(columns=dict(zip(df_features['cell_id'], df_features['donor'])))

    # Somme des valeurs pour les colonnes qui ont le même nom
    chunk = chunk.groupby(chunk.columns, axis=1).sum()

    # Concaténation du morceau traité avec les précédents
    df_counts = pd.concat([df_counts, chunk], axis=1)
    df_counts = df_counts.groupby(df_counts.columns, axis=1).sum()


df_counts.to_csv(os.path.join(args.output_path, args.output_name), sep=",")
# # Initialisation du DataFrame de résultats
# df_counts = pd.DataFrame()
#
# # Parcours des chunks et traitement de chaque morceau
# for chunk in chunks:
#     # Conversion des compteurs en entiers non signés 16 bits pour économiser la mémoire
#     chunk = chunk.astype(np.uint16)
#
#     # Lecture du deuxième fichier CSV contenant les informations de features des échantillons
#     df_features = pd.read_csv('metadata02.csv')
#
#     # Remplacement des noms de colonnes du premier tableau par les features correspondantes
#     chunk = chunk.rename(columns=dict(zip(df_features['cell_id'], df_features['cell_type'])))
#
#     # Concaténation du morceau traité avec les précédents
#     df_counts = pd.concat([df_counts, chunk], axis=1)
#
#     # Somme des valeurs pour les colonnes qui ont le même nom
#     df_counts = df_counts.groupby(df_counts.columns, axis=1).sum()

# Écriture du résultat dans un nouveau fichier CSV
#df_counts.to_csv('comptages_genes_somme.csv')
