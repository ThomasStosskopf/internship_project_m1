import scanpy as sc
from scipy import io
import sys
import argparse
import os
import zipfile
import anndata as ad
# Create a ArgumentParser object
parser = argparse.ArgumentParser(description="Create metadata file frome h5ad file")

# Add an argument for the file name
parser.add_argument('file_name', metavar='file_name', type=str,
                    help='name of the h5ad file to analyse')
parser.add_argument('--output-folder', metavar='output_folder', type=str,
                    help='name of the output directory', default='output_folder')
parser.add_argument('--output-name', metavar='output_name', type=str,
                    help='output file name', default='metadata.csv')
args =  parser.parse_args()

adata = None
print("Metadata will be extract in a few moment...")
# Load file .h5ad
if zipfile.is_zipfile(args.file_name):
    with zipfile.ZipFile(args.file_name, 'r') as zip_ref:
        # Get list of filenames in zip file
        file_list = zip_ref.namelist()
        # Find the file name that ends with ".h5ad"
        h5ad_file_name = next((file_name for file_name in file_list if file_name.endswith('.h5ad')), None)
        if h5ad_file_name is None:
        # If no .h5ad file was found, display an error message and exit
            print("No file .h5ad has been find.")
            exit(1)
        # Extract the .h5ad file
        file_h5ad_to_read = zip_ref.extract(h5ad_file_name)
        adata = ad.read_h5ad(file_h5ad_to_read)
print("Work in progres...")

# Create the output directory if it does not exist
if not os.path.exists(args.output_folder):
    os.makedirs(args.output_folder)

# adata = sc.read_h5ad(args.file_name)
out_dir = args.output_folder

adata = adata.raw.to_adata()  #only if adata has RAW saved and thats what you want!!

# with open(out_dir + '/barcodes.tsv', 'w') as f:
#     for item in adata.obs_names:
#         f.write(item + '\n')
#
# with open(out_dir + '/features.tsv', 'w') as f:
#     for item in ['\t'.join([x,x,'Gene Expression']) for x in adata.var_names]:
#         f.write(item + '\n')
# io.mmwrite(out_dir +'/matrix', adata.X.T)
#counts_transpose.to_csv(os.path.join(args.output, args.output_name), sep=",")
adata.obs.to_csv(os.path.join(args.output_folder, args.output_name), sep=",")
# adata.obs.to_csv(out_dir + '.metadata.csv')
# adata.obs.to_csv(os.path.join(out_dir, '.metadata.csv'), sep=",")
print(f"metadata from {args.file_name} extracted.")
# counts_transpose.to_csv(os.path.join(args.output, args.output_name), sep=",")
