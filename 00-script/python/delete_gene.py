# script to delete gene
"""Python script to delete genes from a table"""
import pandas as pd



def delete_gene(path_to_mito_gene, path_to_count_table):
    """Function to delete gene that are not in a list"""
    mito_gene = pd.read_csv(path_to_mito_gene)
    table_count = pd.read_csv(path_to_count_table)

    print(table_count.shape[0])
    for index, value in table_count['gene_id'].iteritems():

        if value not in mito_gene['mito_gene'].values:
            table_count = table_count.drop(index)
    table_count.reset_index(drop=True, inplace=True)
    table_count.to_csv("human_table_count_only_mito_gene.csv", sep=",")
    return table_count.shape[0]



if __name__ == "__main__":
    print("bite")
    print(delete_gene(path_to_mito_gene = "07-mito_gene/human_mito_gene.csv", path_to_count_table = "06-merge_all_count_file/human_merge_count_file.csv"))
