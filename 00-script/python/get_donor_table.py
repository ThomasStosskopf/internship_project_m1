import argparse
import sys
import os
import pandas as pd
import re

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description="Find the donor table in the metadata file")
# Add arguments for the CSV file name
parser.add_argument('file_name', metavar='file_name', type=str,
                    help='name of the metadata file to analyse')
parser.add_argument('--output-folder', metavar='output_folder', type=str,
                    help='name of the output directory',
                    default='output_folder_donor_table')
parser.add_argument('--output-name', metavar='output_name',
                    type=str,help='output file name', default='counts.csv')
args =  parser.parse_args()

# Read the csv
metadata = pd.read_csv(args.file_name, sep=",")
print(args.file_name)
# Extract cell_id colomn and donor colomn
cell_id = metadata["cell_id"]
donor = metadata["donor"]



# Extract the name of the tissue to put it in the name of the output file.
path_file = args.file_name
name=""
which_file = re.search(r"/([^/]+)_metadata\.", path_file)
if which_file:
    name = which_file.group(1)
else:
    print("No match found")# Spécifier le chemin de sortie pour le fichier CSV


# Ajouter la chaîne de caractères spécifiée dans le troisième argument à la colonne donor
donor = donor + "_" +name

# Créer un nouveau DataFrame avec les colonnes cell_id et donor
donor_table = pd.DataFrame({"cell_id": cell_id, "donor": donor})

print(donor_table)

# Create the output directory if it does not exist
if not os.path.exists(args.output_folder):
    os.makedirs(args.output_folder)

# Écrire le DataFrame dans le fichier CSV
#donor_table.to_csv(output_file, sep=",", index=False)
donor_table.to_csv(os.path.join(args.output_folder, args.output_name), sep =",")
