import os
import regex
import pandas as pd
workdir: os.getcwd()
# Define variables
raw_data_path = "01-raw_data"
SAMPLE = [f for f in os.listdir(raw_data_path) if f.endswith(".h5ad.zip")]
prefix_file = []
for i in range(len(SAMPLE)):
    sample = SAMPLE[i].replace(".h5ad.zip", "")
    prefix_file.append(sample)
species = "human"
print("Raw files that will be analysed: ", prefix_file)

# Define rules
rule all:
    input:
        expand("03-count_from_h5ad_file/{smp}_count.csv", smp=prefix_file), \
        expand("02-matrix_files/{smp}_metadata.csv", smp=prefix_file), \
        expand("04-donor_table/{smp}_donor_table.csv", smp = prefix_file), \
        expand("05-count_modified/{smp}_count_after_change.csv", smp = prefix_file)
        #expand(f"test_directory/test_merge/merge_file.csv")

rule Find_count_table:
    """
    Find the feature count table inside the h5ad file and return the count
    table in a csv format.
    Input: 01-raw_data/{smp}.h5ad.zip
    Output: 03-count_from_h5ad_file/{smp}_count.csv
    """
    input: "01-raw_data/{smp}.h5ad.zip"
    output: "03-count_from_h5ad_file/{smp}_count.csv"
    shell:"""
    python3 00-script/python/find_count_table.py {input} --output 03-count_from_h5ad_file/ --output-name {wildcards.smp}_count.csv
    """


rule metadata_finder:
    """
    Find the metadata from the h5ad file.
    """
    input: "01-raw_data/{smp}.h5ad.zip"
    output: "02-matrix_files/{smp}_metadata.csv"
    shell:"""
    python3 00-script/python/find_metadata.py {input} --output-folder 02-matrix_files/ --output-name {wildcards.smp}_metadata.csv
    """


rule find_donor_table:
    """
    Retrieve a donor table from metadata files that will be used later
    """
    input: "02-matrix_files/{smp}_metadata.csv"
    output: "04-donor_table/{smp}_donor_table.csv"
    shell:"""
    python3 00-script/python/get_donor_table.py {input} --output-folder 04-donor_table/ --output-name {wildcards.smp}_donor_table.csv
    """


rule transform_count_table:
    """
    Replace cell's name by the name of the tissue they come from avec sum values of each
    colonms.
    """
    input:
        file_name = "03-count_from_h5ad_file/{smp}_count.csv",
        file_donor = "04-donor_table/{smp}_donor_table.csv"
    output: "05-count_modified/{smp}_count_after_change.csv"
    shell:"""
    python3 00-script/python/rename_and_sum.py -file_name {input.file_name} -file_donor {input.file_donor} --output_path 05-count_modified/ --output-name {wildcards.smp}_count_after_change.csv
    """


# rule merge_all_count_table:
#     """
#     """
#     input: "test_directory/count_after_trans/"
#     output: "test_directory/test_merge/merge_file.csv"
#     priority: 0
#     shell:"""
#     python3 00-script/python/fusion2.py -directory {input} -output_path test_directory/test_merge/ -output_name merge_file.csv
#     """
